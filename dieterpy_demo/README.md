*Note date: 26/04/2023*

To use Python as a wrapper of GAMS models, you need to install the Python API of GAMS. Please follow the steps below:

1. Activate your conda environment in the terminal/shell.

2. Install GAMS API:

    ```
    pip install gams --find-links [PATH TO GAMS]\api\python\bdist
    ```

   > __Note__: Replace `[PATH TO GAMS]` with the actual path to your GAMS installation.

3. Install `dieterpy`:

    ```
    pip install dieterpy
    ```

4. If you want to use `symbolx`, please install version 0.3.2 or above:

    ```
    pip install symbolx
    ```

5. You may want to install some relevant Python tools to see the plots and notebooks (.ipynb files):

    ```
    pip install jupyter nbconvert plotly
    ```

> __Note__:
This steps works well with GAMS version above 42, if you have a GAMS version below that you can follow this [intructions](https://diw-evu.gitlab.io/dieter_public/dieterpy/gettingstarted/installation.html#installation-of-the-gams-python-api) or install GAMS 42.


For further details, please refer to these two videos: [video1](https://youtu.be/n7L0i5Dc5fM?t=102) and [video2](https://youtu.be/umDSydpkK5w?t=1761).
