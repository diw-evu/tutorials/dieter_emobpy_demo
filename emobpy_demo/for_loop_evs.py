import json
import os
# Initialize seed
from emobpy.tools import set_seed
set_seed()

from emobpy import (Mobility,
                    DataBase,
                    HeatInsulation,
                    BEVspecs,
                    Consumption,
                    Availability,
                    Charging,
                    Export
                    )



create_this_quantity = 1

# Mobility time series creation
for i in range(create_this_quantity):
    m = Mobility(config_folder='config_files')
    m.set_params(
                name_prefix=f"BEV_{i}",
                total_hours=8760, # one year
                time_step_in_hrs=0.25, # 15 minutes
                reference_date="01/01/2020"
                )
    m.set_stats(
                stat_ntrip_path="TripsPerDay.csv",
                stat_dest_path="DepartureDestinationTrip_Worker.csv",
                stat_km_duration_path="DistanceDurationTrip.csv",
                )
    m.set_rules(rule_key="parttime")
    m.run()
    m.save_profile(folder="db", description='Anything')



# Load saved files
DB = DataBase('db')
DB.loadfiles()

# Consumption time series creation
for profile_name in DB.db.keys():
    if DB.db[profile_name]['kind'] == 'driving':
        HI = HeatInsulation(True)
        BEVS = BEVspecs()
        BEVMODEL = BEVS.model(('Volkswagen','ID.3',2020))
        c = Consumption(profile_name, BEVMODEL)
        c.load_setting_mobility(DB)
        c.run(
            heat_insulation=HI,
            weather_country='DE',
            weather_year=2016,
            passenger_mass=75,                   # kg
            passenger_sensible_heat=70,          # W
            passenger_nr=1.5,                    # Passengers per vehicle including driver
            air_cabin_heat_transfer_coef=20,     # W/(m2K). Interior walls
            air_flow = 0.02,                     # m3/s. Ventilation
            driving_cycle_type='WLTC',           # Two options "WLTC" or "EPA"
            road_type=0,                         # For rolling resistance, Zero represents a new road.
            road_slope=0
            )
        c.save_profile('db')

# Load new saved files to the database instance
DB.update()

# Availability time series creation
station_distribution = {                  # Dictionary with charging stations type probability distribution per the purpose of the trip (location or destination)
    'prob_charging_point': {
        'errands':   {'22kW': 0.5,   'none': 0.5},
        'escort':    {'22kW': 0.5,   'none': 0.5},
        'leisure':   {'22kW': 0.5,   'none': 0.5},
        'shopping':  {'22kW': 0.5,   'none': 0.5},
        'home':      {'3.7kW': 0.5,  'none': 0.5},
        'workplace': {'22kW': 0.25,  '11kW': 0.5,    'none': 0.25},  # If the vehicle is at the workplace, it will always find a charging station available (assumption)
        'driving':   {'75kW': 0.005, '150kW': 0.005, 'none': 0.99},  # with the low probability given to fast charging is to ensure fast charging only for very long trips (assumption)
        }, 
    'capacity_charging_point': {                                     # Nominal power rating of charging station in kW
        '22kW': 22,
        '3.7kW': 3.7,
        '11kW': 11,
        '75kW': 75,
        '150kW': 150,
        'none': 0,                                                   # dummy station
        },
}


for profile_name in DB.db.keys():
   if DB.db[profile_name]['kind'] == 'consumption':
       ga = Availability(profile_name, DB)     
       ga.set_scenario(station_distribution)
       ga.soc_init = 0.95 # Change default value
       ga.run()
       ga.save_profile('db')

# Load new saved files to the database instance
DB.update()

# Charging time series creation
charging_strategy = 'immediate' # Options are: 'immediate', 'balanced' and others

failed = {}
for profile_name in DB.db.keys():
   if DB.db[profile_name]['kind'] == 'availability':
        ged = Charging(profile_name)
        ged.load_scenario(DB)
        ged.set_sub_scenario(charging_strategy) 
        ged.run()
        if not ged.success:
            print(f"{ged.name}: failed to generate a profile with charging strategy '{charging_strategy}'")
            failed[ged.name] = [profile_name, charging_strategy]
        else:
            ged.save_profile('db')

with open(os.path.join(DB.folder,'failed_charging.json'), 'w') as f:
    json.dump(failed, f)

# Export to csv in DIETER format
DB.update()
Exp = Export(groupbyhr=True, kwto='MW')
Exp.loaddata(DB)
Exp.to_csv()
Exp.save_files()