*Note date: 26/04/2023*

To install `emobpy` version 0.6.2, please use the following command in the terminal/shell after activating the `Python` environment:

```
pip install emobpy
```

>__Note__ `pandas` package is an important dependency of `emobpy` which may cause conflicts with the latest version 2.0. Therefore, it is recommended to install `pandas` version 1.5.2 or lower by running the following command after installing `emobpy`:

```
pip install pandas==1.5.2
```

For more detailed information about the installation of `emobpy`, please refer to the official documentation at: https://emobpy.readthedocs.io/en/latest/gettingstarted/installation.html

Additionally, a video tutorial about the installation process of `emobpy` can be found at: https://youtu.be/uW1f1eUAc68