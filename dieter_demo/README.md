*Note date: 26/04/2023*

The recommended location to download the latest stable DIETERgms (1.6.1) is at this [link](https://gitlab.com/diw-evu/dieter_public/dieterpy/-/tree/main/dieterpy/templates/base/project_files/model).
You can find more data, such as the input excel files, in this [folder](https://gitlab.com/diw-evu/dieter_public/dieterpy/-/tree/main/dieterpy/templates/base).

To run DIETER.gms directly from GAMSIDE, follow the instructions below:

1. Input excel files can be copied from [`dieterpy_demo/project_files/data_input`](../dieterpy_demo/project_files/data_input) folder. It is important to add an empty column after DE in the 'incidence' matrix in 'spatial' sheet. Otherwise, a division by zero error will appear when running the model.

2. `node_feat(n)` is an important parameter to activate modules. This parameter is located in `DIETER.gms`, edit it accordingly.

3. Configure the `scenario.gms` file properly according to your intended study. For example, if only one country is considered, then the power flow (`F.fx(l,h) = 0`) must be set as zero, as well as Net Transfer Capacity (`NTC.fx(l) = 0`).
