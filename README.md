# DIETER emobpy demo

This demo folder shows how to run and configure [DIETER](https://diw-evu.gitlab.io/dieter_public/dieterpy) with emobpy time series to model battery electric vehicles and the power sector.


1. emobpy time series are located in the ['emobpy_demo/db'](emobpy_demo/db) folder. Copy and paste the content in the excel files that are the input of DIETER or dieterpy.

    a. If you want to use dieterpy choose the [dieterpy_demo](dieterpy_demo) folder.
    
    b. If you want to use DIETERgms choose the [dieter_demo](dieter_demo) folder

2. Please, read these **important notes** to run every folder succesfuly

    - [emobpy_demo notes](emobpy_demo/README.md)
    - [dieterpy_demo notes](dieterpy_demo/README.md)
    - [dieter_demo notes](dieter_demo/README.md)

>__Note__ Please use the issue section of this Gitlab repository to post your questions.